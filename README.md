alpha version!
---
Not use in production!


Image for Web project based on PHP and PostgreSQL

# About
Included:
- CentOS 7
- nginx
- Lua
- crontab
- php 7.1
  - php-cli
  - php-common
  - php-fpm
  - php-gd
  - php-zip
  - php-mbstring
  - php-mcrypt
  - php-opcache
  - php-pdo
  - php-redis
  - php-pear
  - php-pecl-memcache
  - php-pecl-memcached
  - php-pecl-redis
  - php-pecl-xmldiff
  - php-pecl-zip
  - php-soap
  - php-curl
  - php-xml
  - php-apc
  - php-pgsql
  - php-process

This image based on https://www.github.com/egyptianbman/docker-centos-nginx-php/ repository

#Workdir
Default is `/app`

Example run with compose:

```
version: '3'

volumes:
  vendor:
  bundles:

services:

  web:
    image: thedronzik/web-service
    links:
        - redis
        - memcached
        - node-message
    ports:
      - "80:80"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - .:/app
      - vendor:/app/vendor
      - bundles:/app/public/bundles
      - ./configuration/supervisord.d:/etc/supervisord.d
      - ./configuration/cron.d/crontab:/etc/crontab
      - ./files/etc/nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./files/etc/nginx/conf.d/upstream.conf:/etc/nginx/conf.d/upstream.conf
      - ./files/etc/nginx/conf.d/app.conf:/etc/nginx/conf.d/app.conf
    environment:
      - HOSTNAME=localhost
      - VIRTUAL_HOST=application.localhost
    depends_on:
      - redis
      - memcached
      - node-message
```

Example crontab with ENV variable use
```
* * * * * root . /env.sh; /usr/bin/php /app/some_script.php >> /var/log/cron.some_script.log
```